from django.urls import path
from .views import ResumeDetail, ResumeList

urlpatterns = [
    path('<int:pk>/', ResumeDetail.as_view(), name='resume-detail'),
    path('', ResumeList.as_view(), name='resume-list'),
]
