# Generated by Django 4.2 on 2023-04-29 11:28

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_customuser_education_customuser_experience_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customuser',
            name='education',
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='experience',
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='grade',
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='phone',
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='portfolio',
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='salary',
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='specialty',
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='status',
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='title',
        ),
        migrations.CreateModel(
            name='Resume',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('status', models.CharField(choices=[('Open', 'Open'), ('Closed', 'Closed')], max_length=10)),
                ('grade', models.CharField(choices=[('Junior', 'Junior'), ('Senior', 'Senior')], max_length=10)),
                ('specialty', models.CharField(choices=[('IT', 'IT'), ('Finance', 'Finance'), ('Marketing', 'Marketing')], max_length=20)),
                ('salary', models.DecimalField(decimal_places=2, max_digits=8)),
                ('education', models.CharField(max_length=255)),
                ('experience', models.CharField(max_length=255)),
                ('portfolio', models.URLField(max_length=255)),
                ('phone', models.CharField(max_length=20)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
