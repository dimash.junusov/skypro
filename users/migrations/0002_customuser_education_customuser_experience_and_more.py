# Generated by Django 4.2 on 2023-04-29 11:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='education',
            field=models.CharField(default='MIT', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='customuser',
            name='experience',
            field=models.CharField(default='5 years', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='customuser',
            name='grade',
            field=models.CharField(choices=[('Junior', 'Junior'), ('Senior', 'Senior')], default='Senior', max_length=10),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='customuser',
            name='phone',
            field=models.CharField(default='87073003931', max_length=20),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='customuser',
            name='portfolio',
            field=models.URLField(default='', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='customuser',
            name='salary',
            field=models.DecimalField(decimal_places=2, default=4000, max_digits=8),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='customuser',
            name='specialty',
            field=models.CharField(choices=[('IT', 'IT'), ('Finance', 'Finance'), ('Marketing', 'Marketing')], default='Developer', max_length=20),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='customuser',
            name='status',
            field=models.CharField(choices=[('Open', 'Open'), ('Closed', 'Closed')], default='Open', max_length=10),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='customuser',
            name='title',
            field=models.CharField(default='Python Developer', max_length=255),
            preserve_default=False,
        ),
    ]
