from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from .managers import CustomUserManager


class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_("email address"), unique=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.email


class Resume(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    status = models.CharField(max_length=255)
    grade = models.CharField(max_length=255)
    specialty = models.CharField(max_length=255)
    salary = models.DecimalField(max_digits=8, decimal_places=2)
    education = models.CharField(max_length=255)
    experience = models.IntegerField()
    portfolio = models.CharField(max_length=255)
    phone = models.CharField(max_length=20)
