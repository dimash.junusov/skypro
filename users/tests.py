from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from django.contrib.auth import get_user_model
from .models import Resume
from django.test import TestCase, Client

client = Client()


class ResumeTests(APITestCase):
    def setUp(self):
        User = get_user_model()
        self.user = User.objects.create_user(email='testuser@gmail.com', password='testpass')
        self.resume = Resume.objects.create(
            user=self.user,
            title='Test Resume',
            status='Test Status',
            grade='Test Grade',
            specialty='Test Speciality',
            salary=200,
            education="Test MIT",
            experience=9,
            portfolio='Test Portfolio',
            phone='77073003030'
        )
        self.client = APIClient()
        self.client.login(username='testuser@gmail.com', password='testpass')

    def test_get_resume(self):
        url = reverse('resume-detail', kwargs={'pk': 1})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], self.resume.id)

    def test_patch_resume(self):
        data = {'title': 'New Title'}
        url = reverse('resume-detail', kwargs={'pk': 1})
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.resume.refresh_from_db()
        self.assertEqual(self.resume.title, 'New Title')
